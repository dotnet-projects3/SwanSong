import { createSlice } from "@reduxjs/toolkit";
import { login } from "./authenticationApi" 

const initialState = {
  user: {},
  jwtToken: "",
  refreshToken: "",
  isAuthenticated: false,
  loginFailed: false
};

const authenticationSlice = createSlice({
  name: "authentication",
  initialState, 
  reducers: {
    logout: (state) => {
      state.user = null
     // state.jwtToken = null
     // state.refreshToken = null
      state.isAuthenticated = null 
     // localStorage.removeItem('user');
      localStorage.removeItem('jwtToken');
      localStorage.removeItem('refreshToken');
    },  
    updateToken: (token, refreshToken) => (state) => {
   // updateTokens: (state, token, refreshToken) => {
      let a = token
      let b = refreshToken
    }
  },
  extraReducers: {
    [login.pending]: (state) => {
      console.log("Pending");
      return { ...state, loginFailed: false }
    },
    [login.fulfilled]: (state, { payload }) => {
      console.log("Fetched Successfully!");
      //localStorage.setItem('user', JSON.stringify(payload.user));
      localStorage.setItem('jwtToken', JSON.stringify(payload.jwtToken));
      localStorage.setItem('refreshToken', JSON.stringify(payload.refreshToken));
      return { ...state, user: payload.user, isAuthenticated: payload.isAuthenticated };
     // return { ...state, user: payload.user, jwtToken: payload.jwtToken, refreshToken: payload.refreshToken, isAuthenticated: payload.isAuthenticated };
    },
    [login.rejected]: (state) => {
      console.log("Rejected!");
      return { ...state, loginFailed: true }
    },
  },
});
 
export const selectCurrentUser = (state) => state.authentication.user
export const selectIsAuthenticated = (state) => state.authentication.isAuthenticated 
export const selectLoginFailed = (state) => state.authentication.loginFailed
//export const selectRefreshToken = (state) => state.authentication.refreshToken
//export const selectJwtToken = (state) => state.authentication.jwtToken
export const { logout, updateTokens } = authenticationSlice.actions 
export default authenticationSlice.reducer