import { createAsyncThunk } from "@reduxjs/toolkit";
import http from "../../common/apis/HttpCommon";

export const login = createAsyncThunk(
  "authentication/login",
  async (loginRequest) => {  
    const response = await http.post(
      `login`, loginRequest
    );
    return response.data;
  }
);