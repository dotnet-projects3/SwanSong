import { createAsyncThunk } from "@reduxjs/toolkit";
import http from "../../common/apis/HttpCommon";

export const albumSearchByWord = createAsyncThunk(
  "albums/search",
  async (criteria) => {  
    const response = await http.get(
      `albums/search/` + criteria
    );
    return response.data;
  }
);