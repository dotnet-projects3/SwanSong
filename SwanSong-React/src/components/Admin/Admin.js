import * as React from 'react';
import { CssBaseline, Typography } from '@mui/material';
import { useLocation, NavLink } from "react-router-dom"; 
import AdminNavigationBar  from './AdminNavigationBar'
import Albums from './Albums/Albums' 
import AdminHome from './AdminHome'
import Artists from './Artists/Artists';
import Songs from './Songs/Songs'
import Members from './Members/Members'
import LookUps from './LookUps/LookUps'
import "./AdminHome.scss";

const Admin = (props) => {     

  const location = useLocation();
  let page = null;

  switch(location.pathname)
  {
    case "/admin/home": {
      page = <AdminHome />
      break
    }
    case "/admin/artists": {
      page = <Artists />
      break
    }
    case "/admin/albums": {
      page = <Albums />
      break
    }  
    case "/admin/songs": {
      page = <Songs />
      break
    }
    case "/admin/members": {
      page = <Members />
      break
    } 
    case "/admin/lookups": {
      page = <LookUps />
      break
    } 
  }
 
	return (     
	  <div>
      <div className='top-padder'></div>         
      <CssBaseline /> 
      <AdminNavigationBar />      
      { page } 
    </div>
	) 
}
    
export default Admin