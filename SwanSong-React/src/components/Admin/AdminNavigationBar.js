import * as React from 'react'; 
import { Typography, Container, AppBar, Toolbar, Box, Menu, MenuItem, Button, Tooltip, IconButton, Avatar, Stack } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { selectCurrentUser, selectIsAuthenticated, logout } from '../../features/authentication/authenticationSlice' 
import { useHistory, Link } from 'react-router-dom';
import { useSelector, useDispatch } from "react-redux"; 
import "./AdminHome.scss";

const AdminNavigationBar = () => {    
  const history = useHistory() 
  const currentUser = useSelector(selectCurrentUser)
  const dispatch = useDispatch() 
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = (event) => {

    switch(event.currentTarget.innerText)
    {
      case 'ARTISTS': {
        history.push("/admin/artists")
        break
      }
      case 'ALBUMS': {
        history.push("/admin/albums")
        break
      }
      case 'SONGS': {
        history.push("/admin/songs")
        break
      }
      case 'MEMBERS': {
        history.push("/admin/members")
        break
      }
      case 'LOOKUPS': {
        history.push("/admin/lookups")
        break
      }
      default: {
        history.push("/admin/home")
        break
      }
    }
    setAnchorElUser(null);
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = (setting) => {
    switch(setting)
    {
      case 'Profile': {
        break
      }
      case 'Account': {
        break
      }
      case 'Dashboard': {
        break
      }
      case 'Logout': {
        dispatch(logout()) 
        break
      }      
      default: {
        break
      }
    }
    setAnchorElUser(null);
  };

  const pages = ['Artists', 'Albums', 'Songs', 'Members', 'Lookups'];
  const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

	return (     
	  <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters> 
            <Link to="/admin/home"> 
              <Stack direction={{ xs: 'column', sm: 'row' }} spacing={{ xs: 1, sm: 2, md: 2 }}>  
                <img alt="SwanSong" src="/images/swansonglogo2.png" className='logo' />           
                <Typography
                  variant="h6"
                  noWrap 
                  sx={{
                    mr: 2,
                    width: 140,
                    display: { xs: 'none', md: 'flex' },
                    fontFamily: 'Raleway',
                    fontWeight: 700,
                    letterSpacing: '.3rem',
                    color: 'white',
                    textDecoration: 'none',
                    paddingTop: 1.5
                  }}
                >
                  SwanSong
                </Typography>
              </Stack>
            </Link>
          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page} onClick={() => handleCloseNavMenu(page)} >
                  <Typography textAlign="center">{page}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          
          <Box className="menu-box" sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex', alignItems:'center', justifyContent:'center' } }}>
            {pages.map((page) => (
              <Button
                key={page}
                onClick={handleCloseNavMenu}
                sx={{ my: 2, color: 'white', display: 'block' }}
              >
                {page}
              </Button>
            ))}
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar alt="Remy Sharp" src="/images/lz1.jpg" />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: '45px' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              {settings.map((setting) => (
                <MenuItem key={setting} onClick={() => handleCloseUserMenu(setting)}>
                  <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
        </Toolbar>
      </Container>
    </AppBar> 
	) 
}
    
export default AdminNavigationBar