import * as React from 'react';
import { CssBaseline, Typography } from '@mui/material'; 
import AlbumList from './AlbumList'
import "../AdminHome.scss";

const Albums = (props) => {   
	return (     
	  <div>
      <div className='top-padder'></div>         
      <CssBaseline />
      <Typography component="h1" variant="h5">
        Albums
      </Typography>  
      <AlbumList /> 
    </div>
	) 
}
    
export default Albums