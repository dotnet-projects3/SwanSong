import { useSelector } from 'react-redux'
import { Redirect, Route } from 'react-router-dom' 
import { selectCurrentUser, selectIsAuthenticated } from '../../features/authentication/authenticationSlice'
 
export const PrivateRoute = ({ children, ...rest }) => {

  const user = useSelector(selectCurrentUser)
  const isAuthenticated = useSelector(selectIsAuthenticated)    

  return (
    <Route
      {...rest}
      render = {({ location }) => {
        return (user != null && isAuthenticated === true) ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        );
      }}
    />
  );
}